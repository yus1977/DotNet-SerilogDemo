using Exceptionless;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;

namespace SerilogDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.Title = "SerilogDemo";

                #region 加载appsettings配置

                string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                if (String.IsNullOrWhiteSpace(environment))
                    environment = "Production";
                //加载yml格式的配置文件
                var configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    //加入yml配置文件
                    .AddYamlFile("appsettings.yml", optional: true, reloadOnChange: true)
                    .AddYamlFile($"appsettings.{environment}.yml", optional: true, reloadOnChange: true)
                    //加入环境变量到config对象中,环境变量以ASPNETCORE_为前缀
                    .AddEnvironmentVariables("ASPNETCORE_")
                    .AddCommandLine(args)
                    .Build();

                //启动端口配置
                string[] useUrls = configuration.GetSection("UseUrls").Get<string[]>();
                if (useUrls == null || useUrls.Length == 0) useUrls = new string[] { "http://*:5000" };

                #endregion 加载appsettings配置

                #region 配置Serilog

                //#################
                //配置Serilog
                //从配置文件中读取配置并创建Logger
                var loggerConfig = new LoggerConfiguration().ReadFrom.Configuration(configuration);
                //**集成ExceptionlessSink
                //方法1：在Serilog的配置中增加 WriteTo:Name: Exceptionless,不用单独写Exceptionless的配置代码
                //方法2:手动配置，可以定制Exceptionless
                /*            if (configuration["Exceptionless:Enabled"] == "true")
                            {
                                //定制Exceptionless，输出的日志增加一个tag
                                loggerConfig.WriteTo.Exceptionless(b => b.AddTags("My Loging Tag"));
                            }*/
                //方式3：手动配置，使用创建Sink的方式
                //loggerConfig.WriteTo.Sink(new ExceptionlessSink(), LogEventLevel.Verbose);

                //使用上面的配置创建Serilog工厂
                Log.Logger = loggerConfig.CreateLogger();

                //*********
                //集成Exceptionless
                //启动ExceptionlessClient.Default
                //这样就可以在这里记录日志到ExceptionlessClient，不然就要等Startup运行之后才可以记录日志到ExceptionlessClient
                ExceptionlessClient.Default.Configuration.ReadFromConfiguration(configuration);
                ExceptionlessClient.Default.Startup();

                #endregion 配置Serilog

                Log.Information("Lauching...");

                //创建Host
                CreateHostBuilder(args, environment, configuration, useUrls).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Stopped program because of exception");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args, string environment, IConfiguration configuration, string[] useUrls)
        {
            return Host.CreateDefaultBuilder(args)
                    //应用环境变量
                    //.UseEnvironment(environment)

                    //配置系统默认的加载配置动作，增加yaml的configuration配置
                    .ConfigureAppConfiguration(builder =>
                    {
                        builder.AddConfiguration(configuration);
                    })
                    //或者主动注册configuration到DI中
                    /*                    .ConfigureServices((service) =>
                                        {
                                            //service.RemoveAll<IConfiguration>();
                                            service.AddSingleton(configuration);
                                        })*/
                    //应用Serilog
                    .UseSerilog()
                    //创建默认WebHost
                    .ConfigureWebHostDefaults(webBuilder =>
                    {
                        webBuilder
                            //Kestrel使用yml配置对象
                            //.UseConfiguration(configuration)
                            .UseStartup<Startup>()
                            .UseUrls(useUrls);
                    });
        }
    }
}