using Exceptionless;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using SerilogDemo.Filter;

namespace SerilogDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                //全局异常处理Filter
                options.Filters.Add(typeof(GlobalExceptionFilter));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var test = Configuration["Exceptionless:ApiKey"];

            //使用Exeptionless中间件
            app.UseExceptionless(Configuration);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Serilog自带的记录请求记录
            //app.UseSerilogRequestLogging();
            //或更精细定义
            app.UseSerilogRequestLogging(o => o.GetLevel = (context, duration, ex) =>
            {
                if (ex != null || context.Response.StatusCode > 499)
                    return LogEventLevel.Error;

                if (context.Response.StatusCode > 399)
                    return LogEventLevel.Information;

                /*                if (duration < 1000 || context.Request.Path.StartsWithSegments("/api/v2/push"))
                                    return LogEventLevel.Debug;*/

                return LogEventLevel.Information;
            });

            //***********很重要
            //如果不这样处理,Asp.Net Core 3.1 获取不到Post、Put请求的内容
            app.Use((context, next) =>
            {
                context.Request.EnableBuffering();
                return next();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}