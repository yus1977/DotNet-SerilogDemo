# SerilogDemo
Serilog学习

> 安装组件
```c#
Install-Package Serilog.AspNetCore
//签名版的AspNetCore
Install-Package Exceptionless.AspNetCore.Signed
//异步输出日志
Install-Package Serilog.Sinks.Async
//输出到Serilog
Install-Package Serilog.Sinks.Exceptionless
```